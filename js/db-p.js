$(document).ready(function(){
  var config = {
    apiKey: "AIzaSyBlom9wAkjhot5YV0FsqqeQ4mmQ8ghWrqY",
    authDomain: "tienda-final.firebaseapp.com",
    databaseURL: "https://tienda-final.firebaseio.com",
    projectId: "tienda-final",
    storageBucket: "tienda-final.appspot.com",
    messagingSenderId: "513266541111"
  };
  firebase.initializeApp(config);

  var clientedb = firebase.database().ref().child('cliente');
  var productodb = firebase.database().ref().child('producto')
  var dataC;
  var dataP;
  clientedb.once('value',function(snapshot){
    dataC = snapshot.val();
  });
  productodb.once('value',function(snapshot){
    dataP = snapshot.val();
  });
/*::::::::::Clientes::::::::::*/
  clientedb.on('value',function(snapshot){
    $('#clientes').empty();
    var persona = "";
    for(cliente in snapshot.val()){
      persona += '<div class="opcion persona">'+
                 '<div class="check"></div>'+
                 '<p class="nombre">' + cliente + '</p>' +
                 '<div class="close"><span class="glyphicon glyphicon-remove"></span></div>'+
                 '<div class="remove option-p"><span class="glyphicon glyphicon-trash"></span></div><div class="edit option-p"><span class="glyphicon glyphicon-edit"></span></div>'+
                 '</div>'
    }
    $('#clientes').append(persona);
      personas = "";  

  },function(errorObject){
      console.log('error personas:' + errorObject.code);
  });
/*::::::::::Clientes::::::::::*/
/*::::::::::Productos::::::::::*/
  productodb.on('value',function(snapshot){
  
var data = snapshot.val();
$('#productos').empty();
      var productos = "";
      for(producto in snapshot.val()){
        productos += '<div class="opcion producto"> <div class="operadores"> <div id="mas" class="mas"><p> + </p></div> <div class="menos" id="menos"><p> - </p></div> </div>'+
            '<p class="nombre">' + producto + '</p>' +
            '<div class="remove option-p"><span class="glyphicon glyphicon-trash"></span></div><div class="edit option-p"><span class="glyphicon glyphicon-edit"></span></div>'+
            '<div class="valor">' + data[producto].valorProducto + '</div>'+
            '</div>'
      }

      $('#productos').append(productos);
      productos = "";
/*::::::::::suma y resta de los valores del producto::::::::::*/
      var compra = 0;
      var cProducto = 0; 
      $('.mas').on('click',function(){
        if($('.persona').hasClass('active')){
          console.log()
          var numero = $(this).parent().siblings('.valor').text();
          var nombre = $(this).parent().siblings('.nombre').text();
          var precio = parseInt(numero);
          var total = precio + compra;
          compra = total;
          cProducto = cProducto + 1;
          mostrar(total);
          factura(nombre,cProducto);

        }else{
          console.log('aun no seleccionas a un cliente');
        }

      });

      $('.menos').on('click',function(){
        if($('.persona').hasClass('active')){
          if (compra <= 0){
            compra = 0;
            mostrar(total);
          }else{
            var numero = $(this).parent().siblings('.valor').text();
            var precio = parseInt(numero);
            var total = compra - precio ;
            compra = total;
            mostrar(total);
          }
        }else{
         console.log('aun no seleccionas a un cliente'); 
        }
      });

var facturacliente =[];
function factura(producto,unidades){
  facturacliente.push({
    nombre : producto,
    cantidad : unidades
  });


  console.log(facturacliente);
}

/*::::::::::mostrar el valor de tu compra en la 3ser columna::::::::::*/
      function mostrar(valor){
        $('#valor-compra').text(valor);
      }
/*::::::::::mostrar el valor de tu compra en la 3ser columna::::::::::*/

/*::::::::::suma y resta de los valores del producto::::::::::*/
  },function(errorObject){
      console.log('error personas:' + errorObject.code);
  });
/*::::::::::Productos::::::::::*/

/*::::::::::error en la conexion::::::::::*/
var onComplete = function(error){
  if(error){
    console.log(error,'La sincronización fallo');
  }else{
    console.log(error,'La sincronización fue exitosa');
  }
}
/*::::::::::error en la conexion::::::::::*/
/*::::::::::Agregar Cliente::::::::::*/
$('#agregarC').on('click',function(e){
  e.preventDefault();
  var nombreC = $('#nombre').val();
  var cliente = {
      "compra" : $('#compra').val(),
      "afavor" : $('#afavor').val(),
      "deuda"  : $('#deuda').val()
    }
clientedb.once('value',function(snapshot){
  if(snapshot.hasChild(nombreC)){
    alert('error');
  }else{
    clientedb.child(nombreC).set(cliente,onComplete);
    alert('Cliente agregado');
    $('.formularios').toggleClass('active');
  }
});
});
/*::::::::::Agregar Cliente::::::::::*/
/*::::::::::Agregar producto::::::::::*/
$('#agregarP').on('click',function(e){
  e.preventDefault();
  var nombreP = $('#nombreP').val();

  var producto = {
        "cantidadPaquete"  : $('#cantidad').val(),
        "unidadesVendidas" : $('#ganancia').val(),
        "valorProducto"    : $('#valor').val()
      }

productodb.once('value',function(snapshot){
  if(snapshot.hasChild(nombreP)){
    alert('error');
  }else{
    productodb.child(nombreP).set(producto,onComplete);
    alert('Producto agregado');
    $('.formularios').toggleClass('active');
  }
});

});
/*::::::::::Agregar producto::::::::::*/
  setTimeout(function(){ 
/*::::::::::seleccion de perosona::::::::::*/
    $('#cargar-compra').on('click',function(){
      var compra = $('#valor-compra').text();
      var name = $('.persona.active .nombre').text();
      var total;
        var compraAnterior = dataC[name].compra;
        var total = parseInt(compraAnterior) + parseInt(compra);
        clientedb.child(name).update({
          "compra" : total,
          "afavor" : dataC[name].afavor,
          "deuda"  : dataC[name].deuda
        },function(){
          $('#valor-compra').text('0');
        });
    });

    $('#cargar-deuda').on('click',function(){
      var compra = $('#valor-compra').text();
      var name = $('.persona.active .nombre').text();
      var total;
        var compraAnterior = dataC[name].deuda;
        var total = parseInt(compraAnterior) + parseInt(compra);
        clientedb.child(name).update({
          "compra" : dataC[name].compra,
          "afavor" : dataC[name].afavor,
          "deuda"  : total
        },function(){
          $('#valor-compra').text('0');
        });
    });

    $('.edit').click(function(){

if($(this).parent().hasClass('persona')){
  if($(this).parent().hasClass('inhabil')){
    console.log('no se puede');
  }else{
    var name = $(this).parent().text();
    $('.formularios').toggleClass('active');
      $('#nombre').val(name);
      $('#compra').val(dataC[name].compra);
      $('#deuda').val(dataC[name].deuda);
      $('#afavor').val(dataC[name].afavor);
      $('#agregarC').addClass('hidden');
      $('#editarC').addClass('show');

    $('#editarC').on('click',function(){
        clientedb.child(name).update({
          "compra" : $('#compra').val(),
          "afavor" : $('#afavor').val(),
          "deuda"  : $('#deuda').val()
        },function(){
          $('#nombre').val('');
          $('#compra').val('');
          $('#deuda').val('');
          $('#afavor').val('');
          $('#editarC').removeClass('show');
          $('#agregarC').removeClass('hidden');

        });
    });
  }
}else{
  console.log($(this).siblings('.nombre').text());
  var nproducto = $(this).siblings('.nombre').text();
  $('.formularios').toggleClass('active');
  $('#tab-cliente').removeClass('active in');
  $('a[href="#tab-cliente"]').parent().removeClass('active');
  $('#tab-producto').addClass('active in');
  $('a[href="#tab-producto"]').parent().addClass('active');
      $('#nombreP').val(nproducto);
      $('#cantidad').val(dataP[nproducto].cantidadPaquete);
      $('#valor').val(dataP[nproducto].valorProducto);
      $('#agregarP').addClass('hidden');
      $('#editarP').addClass('show');

      $('#editarP').on('click',function(){
        productodb.child(nproducto).update({
          cantidadPaquete: $('#cantidad').val(),
          unidadesVendidas: dataP[nproducto].unidadesVendidas,
          valorProducto : $('#valor').val()
        },function(){
          $('#cantidad').val('');
          $('#valor').val('');
          $('#nombreP').val('');
          $('#agregarP').removeClass('hidden');
          $('#editarP').removeClass('show');
        });
      })
}

    });
    $(".check").click(function(){
      $('.persona').addClass('inhabil');
      $(this).parent().addClass('active').removeClass('inhabil');
      $('.btns-valor').addClass('show');
      var name = $(this).parent().text();
        if(dataC[name].deuda >= 1){
          $('#v-deuda').addClass('show');
          $('#valor-deuda').text(dataC[name].deuda);
        }

        if(dataC[name].afavor >= 1){
          $('#v-afavor').addClass('show');
          $('#valor-afavor').text(dataC[name].afavor);
        }
    });
    $('.close').click(function(){
      $('.persona').removeClass('inhabil');
      $(this).parent().removeClass('active');
      $('#v-deuda').removeClass('show');
      $('#v-afavor').removeClass('show');
      $('#valor-compra').text('0');
    });
/*::::::::::seleccion de perosona::::::::::*/
/*::::::::::Eliminar producto::::::::::*/
    $('.remove').click(function(){
        var name = $(this).parent().text();
        var producto = $(this).siblings('.nombre').text();
        clientedb.child(name).remove();
        productodb.child(producto).remove();
    });
/*::::::::::Eliminar producto::::::::::*/
/*::::::::::Editar persona::::::::::*/
  }, 2000);

  $('#open-close').on('click',function(){
    $('.formularios').toggleClass('active');
  });








































});